package pl.codementors.javafx.homework2;

import javafx.animation.*;
import javafx.application.Application;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;
import javafx.util.Duration;


public class Main extends Application {
    public void start(Stage stage) throws Exception {

        BorderPane rootPane = new BorderPane();
        Scene scene = new Scene(rootPane, 600, 600, Color.BLACK);
        Polygon star = drawStar(rootPane);
        Pane p = new Pane();
        rootPane.setCenter(p);

        VBox vBox = new VBox();
        ToggleButton button = new ToggleButton("Start/Pause");
        button.setUserData(sequentialTransition(star));
        button.setOnAction(event -> {
            ToggleButton toggleButton = (ToggleButton) event.getSource();
            Transition transition = (Transition) toggleButton.getUserData();
            if (toggleButton.isSelected()) {
                transition.play();
            } else {
                transition.pause();
            }
        });
        vBox.getChildren().add(button);
        rootPane.setLeft(vBox);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private Polygon drawStar(BorderPane rootPane) {
        Double[] points = {205.0, 150.0, 217.0, 186.0, 259.0, 186.0,
                223.0, 204.0, 233.0, 246.0, 205.0, 222.0, 177.0, 246.0, 187.0, 204.0,
                151.0, 186.0, 193.0, 186.0};
        Polygon star = new Polygon();
        star.getPoints().addAll(points);
        star.setFill(Color.RED);
        star.setLayoutX(100);
        star.setLayoutY(100);
        rootPane.getChildren().add(star);
        return star;
    }

    private Transition moveUp(Node node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(200, 200));
        path.getElements().add(new LineTo(200, -50));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);

        return pt;
    }

    private Transition moveRight(Node node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(200, -50));
        path.getElements().add(new LineTo(450, 200));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        return pt;
    }

    private Transition moveDown(Node node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(450, 200));
        path.getElements().add(new LineTo(200, 450));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        return pt;
    }

    private Transition moveLeft(Node node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(200, 450));
        path.getElements().add(new LineTo(-50, 200));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        return pt;
    }

    private Transition moveCenter(Node node) {
        Path path = new Path();
        path.getElements().add(new MoveTo(-50, 200));
        path.getElements().add(new LineTo(200, 200));
        PathTransition pt = new PathTransition();
        pt.setDuration(Duration.millis(2000));
        pt.setPath(path);
        pt.setNode(node);
        return pt;
    }

    private Transition rotate(Node node) {
        RotateTransition rt = new RotateTransition();
        rt.setFromAngle(0);
        rt.setToAngle(360);
        rt.setDuration(Duration.millis(2000));
        rt.setNode(node);
        return rt;
    }

    private Transition fade(Node node) {
        FadeTransition ft = new FadeTransition();
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setDuration(Duration.millis(2000));
        ft.setNode(node);
        return ft;
    }

    private Transition show(Node node) {
        FadeTransition ft = new FadeTransition();
        ft.setFromValue(0.1);
        ft.setToValue(1.0);
        ft.setDuration(Duration.millis(2000));
        ft.setNode(node);
        return ft;
    }

    private Transition changeColor(Node node) {
        FillTransition ft = new FillTransition();
        ft.setFromValue(Color.RED);
        ft.setToValue(Color.BLUE);
        ft.setShape((Shape) node);
        ft.setCycleCount(2);
        ft.setAutoReverse(true);
        ft.setDuration(Duration.millis(1000));
        return ft;
    }

    private Transition moveRightAndRotate(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(moveRight(node));
        pt.getChildren().add(rotate(node));
        return pt;
    }

    private Transition moveDownAndFade(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(moveDown(node));
        pt.getChildren().add(fade(node));
        return pt;
    }

    private Transition moveLeftAndShow(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(moveLeft(node));
        pt.getChildren().add(show(node));
        return pt;
    }

    private Transition moveCenterAndChangeColor(Shape node) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().add(moveCenter(node));
        pt.getChildren().add(changeColor(node));
        return pt;
    }

    private Transition sequentialTransition(Shape node) {
        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(moveUp(node),
                moveRightAndRotate(node),
                moveDownAndFade(node),
                moveLeftAndShow(node),
                moveCenterAndChangeColor(node));

        st.setCycleCount(Timeline.INDEFINITE);
        return st;
    }

}
